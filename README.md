# Tooter

Tooter is an open source social network powered by the Tooter Platform. It is the new social network of Tooter.in.

## License

Copyright (C) 2019 Gab AI, Inc., and other contributors (see [AUTHORS.md](AUTHORS.md), forked)

Copyright (C) 2016-2019 Eugen Rochko & other Gab Social contributors

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
