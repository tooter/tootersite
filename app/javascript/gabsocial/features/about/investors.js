import Block from '../../components/block'
import Button from '../../components/button'
import Divider from '../../components/divider'
import Heading from '../../components/heading'
import Text from '../../components/text'

export default class Investors extends PureComponent {

  render() {

    return (
      <div className={[_s.default].join(' ')}>
        <Block>
          <div className={[_s.default, _s.heightMin50VH, _s.px15, _s.py15].join(' ')}>
            <Heading>Investors</Heading>
            <br />

            <Text tagName='p' className={_s.mt15} size='medium'>For investment opportunities at Tooter, reach out to us at:</Text>
            <Text tagName='p' className={_s.mt15} size='medium'>
              Tooter Pvt. Ltd.<br />
              Plot No. 48, SriCity, Barugudem<br />
              Khammam, TE 507163
            </Text>
          </div>
        </Block>
      </div>
    )
  }

}
