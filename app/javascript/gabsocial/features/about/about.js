import Block from '../../components/block'
import Button from '../../components/button'
import Divider from '../../components/divider'
import Heading from '../../components/heading'
import Text from '../../components/text'

export default class About extends PureComponent {

  render() {

    return (
      <div className={[_s.default].join(' ')}>
        <Block>
          <div className={[_s.default, _s.px15, _s.py15, _s.mb10].join(' ')}>
            <Heading>About Tooter</Heading>
            
            <Text tagName='p' className={_s.mt15} size='medium'>We believe that India should have a Swadeshi social network. Without one we are just a digital colony of the American Twitter India Company, no different than what we were under the British East India Company. Tooter is our Swadeshi Andolan 2.0. Join us in this Andolan. Join us!</Text>
            
            <Text tagName='p' className={_s.mt15} size='medium'>
              For full terms and conditions of use of this site please see&nbsp;
              <Button
                isText
                underlineOnHover
                color='brand'
                backgroundColor='none'
                className={_s.displayInline}
                to='/about/tos'
              >
                Terms of Service
              </Button>.
            </Text>
          </div>

          <Divider />

          <div className={[_s.default, _s.px15, _s.py15].join(' ')} id='opensource'>
            <Heading>Open Source</Heading>

            <Text tagName='p' className={_s.mt15} size='medium'>Originally forked from the Mastodon project, Tooter's codebase is free and open-source, licensed under the GNU Affero General Public License version 3 (AGPL3).</Text>
            <Text tagName='p' className={_s.mt15} size='medium'>As a result, you, the user, have a choice when using Tooter: you can either have an account on Tooter.in, or, if you don't like what we're doing on Tooter.in or simply want to manage your own experience, you can spin up your own Tooter server that you control, that allows you to communicate with millions of users on their own federated servers from around the world, including users on Tooter.</Text>
            <Text tagName='p' className={_s.mt15} size='medium'>You can download the complete source code of Tooter from:</Text>

            <Text tagName='p' className={_s.mt15} size='medium'>
              <Button
                isText
                underlineOnHover
                color='brand'
                backgroundColor='none'
                className={_s.displayInline}
                href='https://code.gab.com/gab/social/gab-social'
              >
                GitLab
              </Button>
            </Text>
          </div>
        </Block>
      </div>
    )
  }

}
